from django.db import models


class statusmessage(models.Model):
    status = models.TextField(max_length=300)
    date_created = models.DateTimeField(auto_now_add=True)