from django.forms import ModelForm
from django import forms
from .models import statusmessage


class statusmessage_form(ModelForm):
    status = forms.CharField(max_length = 300, required = True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    
    class Meta:
        model = statusmessage
        widget = {'status': forms.TextInput(attrs={'class': 'form-control'})}
        fields = ['status']