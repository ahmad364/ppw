from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, profile
from .models import statusmessage
from .forms import statusmessage_form
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest


class story_7_functional_test(TestCase):
    """docstring for story_6_functional_test"""

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        super(story_7_functional_test, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(story_7_functional_test, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://sahabat-ngedit.herokuapp.com/story_6/beranda/')
        # find the form element
        status = selenium.find_element_by_id('id_status')

        submit = selenium.find_element_by_name('submit')

        # Fill the form with data
        status.send_keys('Coba Coba')

        # submitting the form
        submit.send_keys(Keys.RETURN)

    def test_position_footer_fixed(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://sahabat-ngedit.herokuapp.com/story_6/beranda/')
        # find the form element
        fixed_bottom = self.selenium.find_element_by_class_name("fixed-bottom").value_of_css_property("position")
        self.assertEqual(fixed_bottom, "fixed")

    def test_position_footer_bottom(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://sahabat-ngedit.herokuapp.com/story_6/beranda/')
        # find the form element
        fixed_bottom = self.selenium.find_element_by_class_name("fixed-bottom").value_of_css_property("bottom")
        self.assertEqual(fixed_bottom, "0px")


class story_6UnitTest(TestCase):

    def test_story_6_url_is_exist(self):
        response = Client().get('/story_6/beranda/')
        self.assertEqual(response.status_code, 200)

    def test_story_6_using_index_function(self):
        found = resolve('/story_6/beranda/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_statusmessage(self):
        statusmessage.objects.create(status='Hello, Apa kabar ?')
        counting_all_available_statusmessage = statusmessage.objects.all().count()
        self.assertEqual(counting_all_available_statusmessage, 1)

    def test_form_statusmessage_input_has_placeholder_and_css_classes(self):
        form = statusmessage_form()
        self.assertIn('class="form-control', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = statusmessage_form(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )

    def test_story_6_post_success_and_render_the_result(self):
        test = 'apa aja'
        response_post = Client().post('/story_6/beranda/', {'status': test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/story_6/beranda/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story_6_post_error_and_render_the_result(self):
        test = 'apa saja' * 300
        Client().post("/", {"status": test})
        Client().post('/story_6/beranda/', {'status': ''})
        self.assertEqual(statusmessage.objects.filter(status=test).count(), 0)

    def test_story_6_profile_is_exist(self):
        response = Client().get('/story_6/profile/')
        self.assertEqual(response.status_code, 200)

    def test_story_6_using_profile_function(self):
        found = resolve('/story_6/profile/')
        self.assertEqual(found.func, profile)

    def test_story_6_create_content_profile(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf-8')
        self.assertIn('<title> My Profile </title>', html_response)
