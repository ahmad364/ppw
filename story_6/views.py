from django.shortcuts import render
from .forms import statusmessage_form
from .models import statusmessage
from django.http import HttpResponseRedirect
# Create your views here.

response = {}


def index(request):
    message = statusmessage.objects.all()
    if request.method == 'POST':
        form = statusmessage_form(request.POST)
        if form.is_valid():
            response['status'] = request.POST['status']
            status = statusmessage(status=response['status'])
            status.save()
            return HttpResponseRedirect('/story_6/beranda/')

    else:
        form = statusmessage_form()
    return render(request, 'status_message.html', {'form': form, 'message': message})


def profile(request):
    return render(request, 'profile.html')
